# vue-mock-json5
> 这是一个vue中运行mock的项目，跟我之前写的node-mock不同，node-mock是独立运行，而此项目的mock是跟随vue项目启动；跟vue-mock不一样的地方在于，这个项目用了json5

## Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## 使用说明
1. 初始化
```
npm i
```
```
npm run serve
```

## 责任声明
仅供学习参考，请勿用于商业，如有问题，概不负责。
