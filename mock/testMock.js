// 引入mockjs
const { mock } = require('mockjs')
const Mock = require('mockjs')

let id = Mock.mock('@id')
console.log(id)

var obj  = Mock.mock({
    id: '@id',
    username: '@cname',
    date: '@date',
    avatar: "@image('200x100', '#50B347', '#FFF', 'Mock.js')", //参数：大小；背景；foreground（文字颜色）；文字 
    description: '@paragraph', // 描述
    ip: "@ip()", // ip地址
    email: "@email"
})

console.log(obj)