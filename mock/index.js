const fs = require('fs')
const path = require('path')
const Mock = require('mockjs')
const JSON5 = require('json5');

//读取json文件
function getJsonFile(filePath){
    //读取指定的json文件
    var json = fs.readFileSync(path.resolve(__dirname, filePath), 'utf-8');
    // 解析并返回
    return JSON5.parse(json);
}

//返回一个函数
module.exports = function(app){
    if(process.env.Mock == "true"){// 对环境变量进行一个判断，方便我们替换真实接口
        //监听http请求
        app.get('/user/userinfo', function(req, res) {
            //每次响应请求时读取mock data的json文件
            //getJsonFile方法定义了如何读取json文件并解析成数据对象
            var json = getJsonFile('./userinfo.json5')
            //将json文件传入Mock.mock 方法中，生成的数据返回给浏览器
            res.json(Mock.mock(json));
        });
    }
}
